# 构建docker镜像
# 基础镜像使用Java
FROM openjdk:8-jdk-alpine
# 作者
MAINTAINER cxh
# VOLUME指定了临时文件目录为/tmp
# 其效果是在主机 /var/lib/docker 目录下创建了一个临时文件，并链接到容器的/tmp
VOLUME /tmp
# 将jar包添加到容器中并更名为app.jar
COPY target/cicdtest-0.0.1-SNAPSHOT.jar /app/app.jar

#COPY target/lib /app/lib

ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","-Dspring.profiles.active=prod","/app/app.jar"]