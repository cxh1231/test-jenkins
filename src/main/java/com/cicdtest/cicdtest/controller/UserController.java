package com.cicdtest.cicdtest.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/user")
public class UserController {


    @GetMapping(value = "/getInfo/{id}")
    public String getInfo(@PathVariable String id){
        return "获取ID为"+id+"的用户的信息";
    }
}
