package com.cicdtest.cicdtest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class CicdtestApplication {


    @GetMapping("/")
    String home() {
        return "2021年6月18日22:31:20<br>" +
                "2021年6月18日22:33:33 增加了这一行";
    }


    @GetMapping("/test")
    String home2() {
        return "这里是我写的测试内容";
    }

    @GetMapping("/test2")
    String home3() {
        return "{'id':'1010'}";
    }

    public static void main(String[] args) {
        SpringApplication.run(CicdtestApplication.class, args);
    }

}
